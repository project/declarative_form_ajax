# Declarative Form AJAX

This module provides a simpler way for form elements to be updated via AJAX in
response to another element having its value changed.

Similarly to core's form states system, this uses a declarative syntax without
any callbacks.

This is a proof of concept for a core issue, but can also be used as a contrib
dependency.

## Requirements

This module requires no modules outside of Drupal core.

## Demo

Enable the declarative_form_ajax_demo module and go to:

- /demo/declarative-ajax-form for a demo with simple form element.
- /demo/declarative-ajax-element-form with a custom form element.

## Usage

In the following example, the container element will be updated via AJAX when
the controlling checkbox has its value changed by the user:

```
$form['controlling_element'] = [
  '#type' => 'checkbox',
  '#title' => 'Click me',
];

// This container is automatically updated when the user changes the value of
// the controlling_element checkbox.
$form['my_container'] = [
  '#type' => 'container',
  '#ajax' => [
    'updated_by' => [
      // Form parents of the controlling element.
      ['controlling_element'],
    ],
  ],
];

// Set this after build callback on the whole form to set up the AJAX
// behaviours.
$form['#after_build'][] = FormAjax::class .  '::ajaxAfterBuild';

```

See the demo module's code for further examples.
